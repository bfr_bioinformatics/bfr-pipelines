# bfr-pipelines

Instructions for installation and interoperability of BfR Pipelines for short read analysis


# overview

The BfR bioinformatics unit developed a set of pipelines for the detailed analysis of bacterial isolate data from short reads. The pipelines are

* [AQUAMIS](https://gitlab.com/bfr_bioinformatics/AQUAMIS) for de-novo assembly and quality control
* [bakcharak](https://gitlab.com/bfr_bioinformatics/bakcharak) for bacterial characterization
* [chewieSnake](https://gitlab.com/bfr_bioinformatics/chewieSnake) for cgMLST analysis
* [snippySnake](https://gitlab.com/bfr_bioinformatics/snippySnake) for SNP analysis

Typically, the intended use scenario is as follows
* Run AQUAMIS on an entire sequencing run
* Collect all assemblies of samples from a species or genus in an assembly sample sheet (also from different sequencing runs) and perform characterization with bakcharak
* Collect all assemblies of samples from a species or genus in an assembly sample sheet (also from different sequencing runs) and perform cgMLST phylogeny with chewieSnake
* Collect trimmed reads (in a tab-separated sample sheet with columns "sample fq1 fq2") that are found to be phylogenetically related (via chewieSnake cgMLT or bakcharak MLST) and perform a SNP analysis for resolving the detailed phylogenetic relationship


# installation

Details to all pipeline's installation can be found in the respective repositories:
* [AQUAMIS](https://gitlab.com/bfr_bioinformatics/AQUAMIS): _clone repo and install depdendencies with conda_
* [bakcharak](https://gitlab.com/bfr_bioinformatics/bakcharak): _clone repo and install depdendencies with conda_
* [chewieSnake](https://gitlab.com/bfr_bioinformatics/chewieSnake): _conda package and docker container available_
* [snippySnake](https://gitlab.com/bfr_bioinformatics/snippySnake): _clone repo and install depdendencies with conda_

There, you always find the latest information regarding the installation.

## prerequisites

The pipelines each depend on a large number of bioinformatic software tools. The correct installation can be most easily done using the conda package management system.

_ Docker packages are under construction _ 

Furthermore, 
* the `git` file versioning software needs to be available
* the installation and execution of the software requires internet access - therefore possibly web proxies must be set up properly

### conda preparation

1. install _miniconda_, see [here](https://docs.conda.io/projects/conda/en/latest/user-guide/install/index.html) for details

2. install the _mamba_ package in the base environment

```
conda install mamba
```

Now you are set to install each pipeline's depdendencies using the `mamba env create -c conda-forge -f <path_to_pipeline>/envs/myenv.yaml` command.

### Pipeline details

### AQUAMIS

To install the latest stable version of AQUAMIS, please clone the git repository on your system.

```
cd <path_to_installation>
git clone https://gitlab.com/bfr_bioinformatics/AQUAMIS.git
```

Next, please execute the script:

```
<path_to_aquamis>/scripts/aquamis_setup.sh
```

to install the conda dependency manager mamba, create the conda environment aquamis and install external databases within the default folder <path_to_aquamis>/reference_db.

Manual Conda Environment Setup

Alternatively, please initialize a conda base environment containing snakemake and mamba (mamba is faster in resolving dependencies), then:

```
mamba env create -n aquamis -f <path_to_aquamis>/envs/aquamis.yaml
```

This creates an environment named aquamis containing all dependencies.
It is found under <path_to_conda>/envs/aquamis.

### bakcharak

Create directory for this repository

```
REPO_PATH="path/2/parent" # select a path of your choice; bakcharak will be available below this path
mkdir -p $REPO_PATH # make sure that path exists
```

Clone this repository

```
cd $REPO_PATH
git clone https://gitlab.com/bfr_bioinformatics/bakcharak
```

Install conda environment

```
mamba env create -n bakcharak --file=$REPO_PATH/bakcharak/envs/bakcharak.yaml
```

Set up databases

Please try the script

```
$REPO_PATH/bakcharak/database_setup.sh $REPO_PATH/bakcharak
```

### chewieSnake


```
mamba create -c bioconda -n chewiesnake chewiesnake
```

### snippySnake

#### Step 1: Get the source code by cloning this repisitory

```
git clone https://gitlab.com/bfr_bioinformatics/snippySnake
```

to a directory of your choice (path/2/repo).

#### Step 2: Create a conda environment

```
mamba env create -n snippysnake -f envs/snippysnake.yaml
```

### Verifying your installation

1. Activate the conda environment and run the respective wrapper with the `--help` option (e.g. `path/2/aquamis.py --help`)
2. The pipelines contain test data. Download the test data and run a test run as explained in the README files.

### Updating the installation

The latest version can be obtained by pulling from the repository's gitlab site. If the software depdendencies were not modified, no installation of the conda packages is required. Need for conda updates should be visible from _tags_.









